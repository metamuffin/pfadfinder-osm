function PfadfinderEngine(id, h_scale) {
  return {
    id,
    creditline: "<a href=\"https://codeberg.org/metamuffin/pfadfinder/\" target=\"_blank\">Pfadfinder</a>",
    draggable: false,
    getRoute: async (points, callback) => {
      const convert = p => ({ lat: p.lat, lng: p.lon })

      const api_base = `${window.location.protocol}//${window.location.hostname}:28159`
      const start_p = points[0]
      const end_p = points[1]
      const start_r = await fetch(api_base + `/query_location?lat=${start_p.lat}&lon=${start_p.lng}`)
      const end_r = await fetch(api_base + `/query_location?lat=${end_p.lat}&lon=${end_p.lng}`)
      const start = await start_r.json()
      const end = await end_r.json()

      if (!start || !end) {
        console.error("points not found", start, end);
        return callback(true)
      }

      const res = await fetch(api_base + `/path?start=${start.id}&end=${end.id}&h_scale=${h_scale}`)
      if (!res.ok) {
        console.error("requets failed");
        return callback(true);
      }

      const route = await res.json()
      const line = route.waypoints.map(p => [p.lat, p.lon])

      const steps = [];
      for (const inst of route.description.instructions) {
        const wp = convert(route.waypoints[inst.index])
        steps.push([
          wp,
          0,
          inst.text,
          1,
          [wp],
        ]);
      }

      callback(false, {
        line,
        steps,
        distance: route.description.distance,
        time: route.description.duration,
      });
    },
    error: function () {
      callback(true);
    }
  }
}

OSM.Directions.addEngine(new PfadfinderEngine("pfadfinder", 0.5), true);
OSM.Directions.addEngine(new PfadfinderEngine("pfadfinder-fast", 2), true);
